# README #
This Python script allows the user to quickly create POIs, delete POIs and create and delete associations for POIs.

This page describes the steps necessary to run the POI script on your local machine.

If you encounter any issues with this script please email mustansir.taquee@wiliot.com
* Other community or team contact