from getpass import getpass

from wiliot.cloud_apis.traceability.traceability import *
from wiliot.cloud_apis.management.management import *
import requests
import csv
import random
import string
import os
from datetime import datetime
from tkinter import *
from tkinter import filedialog
import pandas as pd

import openpyxl
# added function
def parseData():
    filepath=filedialog.askopenfilename()
    file = pd.read_excel(filepath)
    for index, row in file.iterrows():
        if row['action'] != 'get_pois' and row['action'] != 'get_poi' and row['action'] != 'get_poi_associations' and \
                row['action'] != 'delete_poi' and row['action'] != 'create_poi' and row[
            'action'] != 'create_poi_association' and row['action'] != 'delete_poi_association' and row[
            'action'] != 'delete_poi_associations':
            print("Invalid action found at line ", index + 2, "\n")
            print(
                "Valid actions are: get_pois\n get_poi\n get_poi_associations\n delete_poi\n create_poi\n create_poi_association\n delete_poi_associations\n delete_poi_associations")
    username = input("Please enter your username: ")

    password = getpass(prompt="Please enter your password: ")

    ownerID = input('Please enter ownerID: ')



    tc = TraceabilityClient(username, password, ownerID)
    manclient = ManagementClient(username, password, ownerID)
    for index, row in file.iterrows():

        # prints a list of every POI associated with a ownerID
        if row['action'] == 'get_pois':
            try:
                x = tc.get_pois()
                print("Line: ", index+2, "\n", x)
            except WiliotCloudError as e:
                print(e, "\n")
            continue

        # prints info for a specific POI
        elif row['action'] == 'get_poi' and row['poiId'] !='NaN' and row['poiId'] != '': #get POI by ID
            try:
                y = tc.get_poi(row['poiId'])
                print("Line: ", index +2, "\n", y)
            except WiliotCloudError as e:
                print(e, " in line ", index+2, "\n")
            continue

        # prints all the associations of a poi
        elif row['action'] == 'get_poi_associations': #get poi associations
            if row['poiId'] == 'NaN':
                break
            try:
                a = tc.get_poi_associations(poi_id=row['poiId'])
                print("Line: " , index+2,"\n",  a)
            except WiliotCloudError as e:
                print(e, "in line ", index+2, "\n")

        # deletes a POI
        elif row['action'] == 'delete_poi' and row['poiId'] != 'NaN' and row['poiId'] != '': #delete POI
            try:
                b = tc.delete_poi(row['poiId'])
                print("Successfully deleted POI: ", row['poiId'], "\n")
            except WiliotCloudError as e:
                print(e, "in line ", index+2, "\n")
            continue

        # creates a new POI
        elif row['action'] == 'create_poi' and row['poiId'] != 'NaN' and row['poiName'] != 'NaN' and row['poiId'] != '' and row['poiName'] != '':
            try:
                c = tc.create_poi(row['poiName'], row['poiId'])
                print("POI created successfully: ", c, "\n")
            except WiliotCloudError as e:
                print(e, row['poiName'], "in line ", index+2, "\n")

            continue

        # creates associations for a specific POI
        elif row['action'] == 'create_poi_association' and row['AssociationType'] != 'NaN' and row['associationValue'] != 'NaN': #create Association
            if ',' in row['associationValue']:
                x = row['associationValue'].replace(' ', '')
                result = x.split(',')

                for x in result:
                    try:
                        gw_info = manclient.get_gateway_details(gateway_id=x)
                    except:
                        print("Error, gateway", x, " not found under ownerID." "Check line ",
                              index + 2, "in excel file\n")
                    continue

                    try:
                        d = tc.create_poi_association(poi_id=row['poiId'], association_type=row['AssociationType'],
                                                      association_value=x)
                        print("Association created successfully ", d, "\n")
                    except WiliotCloudError as e:
                        print(e, "in line ", index + 2, "\n")
                    continue
            else:
                try:
                    gw_info = manclient.get_gateway_details(gateway_id=row['associationValue'])
                except:
                    print("Error, gateway", row['associationValue'], " not found under ownerID." "Check line ", index+2, "in excel file\n")
                continue

                try:
                    d = tc.create_poi_association(poi_id=row['poiId'], association_type=row['AssociationType'], association_value=row['associationValue'])
                    print("Association created successfully ", d, "\n")
                except WiliotCloudError as e:
                    print(e, "in line ", index+2, "\n")
                continue

        # deletes ALL associations for a specific POI
        elif row['action'] == 'delete_poi_associations' and row['poiId'] != 'NaN': #delete all assocations for a poi
            try:
                e = tc.delete_poi_associations(ownerID, row['poiId'])
                print(e, "Successfully deleted!", "\n")
            except WiliotCloudError as e:
                print(e, "in line ", index+2, "\n")
            continue

        # deletes a specific association for a specific POI
        elif row['action'] == 'delete_poi_association' and row['poiId'] != 'NaN' and row['associationValue'] != 'NaN':
            try:
                f = tc.delete_poi_association(poi_id=row['poiId'], association_value=row['associationValue'])
                print(f, "Successfully deleted!", "\n")
            except WiliotCloudError as e:
                print(e, "in line ", index+2, "\n")
            continue
        else:
            # add list of actions
            break
    print("\nProgram complete. Goodbye.\n")
    quit()

window = Tk()
button = Button(text="Select File",command=parseData)
button.pack()
window.mainloop()

